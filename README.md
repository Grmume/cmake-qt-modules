# cmake-qt-modules
Demo project to show how to integrate QML modules with a statically linked library.

## Getting started
This repository consists of a simple "main project" and a separate module (which can be reused).
It uses the qt cmake macros to integrate the module (i.e. qt_add_qml_module(...))

The main executable target links to the module via target_link_library.
The module produces a library target called "moduleplugin".