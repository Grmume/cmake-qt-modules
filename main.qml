import QtQuick
import QtQuick.Controls
import QtQuick.Window

import ModuleUri

ApplicationWindow {
    visible: true
    width: 1024
    height: 768
    title: qsTr("Hello World")
    Component.onCompleted: {
        console.log("Hello")
    }

    Module {
        anchors.centerIn: parent
        //anchors.fill: parent
    }

}
