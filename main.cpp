#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <QtQml/qqmlextensionplugin.h>

Q_IMPORT_QML_PLUGIN(ModuleUriPlugin)

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    engine.addImportPath(":/ModulePrefix/");
    engine.load(QUrl("qrc:/main/main.qml"));
    return app.exec();
}
